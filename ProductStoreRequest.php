<?php

namespace App\Http\Requests\Admin\Content\Product;

use App\Helpers\LanguageHelpers;
use App\Http\Requests\BaseRequests;

class ProductStoreRequest extends BaseRequests
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|min:6',
            'img' => 'required|string',
            'cost' => 'required|numeric',
            'description' => 'required|string'
        ];

        //        $rules =  app(LanguageHelpers::class)->getRules(
//            [
//                'title' => 'required|string|min:6',
//                'img' => 'required|string',
//                'cost' => 'required|numeric',
//                'description' => 'required|string'
//            ]);

//        return array_merge($rules->rule, [
//            'link' => ['required', 'string'],
//            'tags' => ['required', 'array'],
//            'tags.*' => ['required', 'numeric', 'exists:portfolio,id'],
//            'img' => ['required', 'string'],
//        ]);
    }

//    public function messages()
//    {
//        $answer =  app(LanguageHelpers::class)->getAnswer([
//            'title.required' => 'Укажите название продукта на %local%',
//            'title.string' => 'Название продукта на %local% должно быть строкой!',
//            'title.min' => 'Минимальная длина на %local% должна быть не меньше 6 символов!',
//        ]);
//
//        return $answer->answer;
//    }
}

