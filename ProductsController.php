<?php

namespace App\Http\Controllers\Admin\Content;

use App\Http\ApiController;
//use App\Models\Portfolio;
//use App\Models\PortfolioTags;
use App\Http\Requests\Admin\Content\Product\ProductTranslateRequest;
use App\Http\Requests\Admin\Content\Product\ProductUpdateRequest;
use App\Models\Portfolio;
use App\Models\Products;

use App\Transformer\Admin\Content\ProductsWhitOutTranslateTransformer;
use Illuminate\Http\Request;
use App\Helpers\LanguageHelpers;
use App\Models\Translation\ProductsTranslation;
use App\Transformer\Admin\Content\ProductsTransformer;
use App\Http\Requests\Admin\Content\Product\ProductStoreRequest;


class ProductsController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {//        $products = Products::with('locale')
//            ->orderByDesc('id')
//            ->paginate(20);
        $products = Products::orderByDesc('id')
            ->paginate(20);
//        dd($products);

//        file_put_contents('index-trans.txt', print_r($this->transform($products, ProductsTransformer::class), true));
//        dd($this->transform($products, ProductsTransformer::class));
        return $this->json($this->transform($products, ProductsTransformer::class));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductStoreRequest $request)
    {


        $product = Products::create([
            'name' => $request->title,
            'img' => $request->img,
            'description' => $request->description,
            'cost' => $request->cost
        ]);

//        $language = app(LanguageHelpers::class)->getLanguage();
//        foreach ($language as $item){
//            $name = 'title_'.$item->title;
//            ProductsTranslation::create([
//                'item_id' => $product->id,
//                'language_id' => $item->id,
//                'name' => $request->$name,
//            ]);
//        }

        return $this->json([
            'message' => 'Продукт успешно создан'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Products::select(['name', 'id', 'created_at', 'img', 'cost', 'description'])
            ->with('translate.language')
            ->findOrFail($id);

        return $this->json($this->transform($product, ProductsTransformer::class));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function edit(Products $products)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */

    public function update(ProductUpdateRequest $request, Products $product)
    {
//        $input = $request->all();
//        dd($input);

        $product->update($request->all());

        return $this->json([
            'message' => 'Продукт успешно обновлен'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Products::destroy($id);
        ProductsTranslation::where('item_id', $id)->delete();

//        $products->delete();

        return $this->json([
            'message' => 'Успешно удаленно'
        ]);
    }

    public function changeTranslate(ProductTranslateRequest $request, Products $product) {

        $lang_id = app(LanguageHelpers::class)->getLanguageId($request->lang);



        if($request->only('title')) {
            ProductsTranslation::updateOrCreate([
                'item_id' => $product->id,
                'language_id' => $lang_id,
            ], [
                'name' => $request->title,
            ]);
        } elseif ($request->only('description')) {
            ProductsTranslation::updateOrCreate([
                'item_id' => $product->id,
                'language_id' => $lang_id,
            ], [
                'description' => $request->description,
            ]);
        }


        return $this->json(['message' => 'Язык '.$request->lang.' успешно обновился!']);
    }
}
