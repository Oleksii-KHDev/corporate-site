<?php

namespace App\Transformer\Admin\Content;
use App\Models\Products;
use Illuminate\Support\Collection;
use League\Fractal;

class ProductsTransformer extends Fractal\TransformerAbstract
{
    public function transform(Products $products)
    {
        file_put_contents('transform.txt', print_r($products, true) );
        $resp = [
            'id' => $products->id,
            'name' => $products->locale ? $products->locale->name : $products->name,
            'img' => $products->img,
            'description' => $products->locale ? $products->locale->description : $products->description,
            'cost' => $products->cost
        ];

        if ($products->translate){
            $resp['translate'] = $this->translate($products->translate);
        }

        if ($products->created_at){
            $resp['created_at'] = $products->created_at->format(config('app.date_format'));
        }

        file_put_contents('resp.txt', print_r($products, true) );

        return $resp;
    }

    protected function translate(Collection $translate) {

            $resp = [];

            foreach ($translate as $item) {
                    file_put_contents('translate_item.txt', print_r($item, true));

                    if(isset($item->language_id)) {
                        $lang['language_id'] = $item->language_id;
                    }

                    if(isset($item->language)) {
                        $lang['language'] = $item->language->title;
                        $lang['language_icon'] = $item->language->icon;
                    }

                    if(isset($item->name)) {
                        $lang['name'] = $item->name;
                    }

                    if(isset($item->description)) {
                        $lang['description'] = $item->description;
                    }

                    $resp[] = $lang;
//                    $resp[] = [
//                        'language_id' => $item->language_id,
//                        'language' => $item->language->title,
//                        'language_icon' => $item->language->icon,
//                        'name' => $item->name,
//                        'description' => $item->description,
//                    ];
                }


//        if(!isset($resp)){
//            $resp = [];
//        }

            return $resp;
    }
}
