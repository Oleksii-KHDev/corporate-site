import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import MyApi from "../../../actions/MyApi";
import {Button, Form, Input, Select, Upload} from "antd";
import Alerts from "../../../Ui/Alerts";
import {LoadingOutlined, PlusOutlined} from "@ant-design/icons";
import { InputNumber } from 'antd';

const CreateProduct = () => {
    // const {tags} = useSelector(state => state.tag)
    const [state, setState] = useState({
        loading: false,
        imageUrl: false,
    });
    const [form] = Form.useForm();
    const {language, success} = useSelector(state => state.global)
    const dispatch = useDispatch()

    useEffect(()=>{
        // dispatch(MyApi({
        //     url: 'admin/content/tags/all',
        //     method: 'GET',
        //     onSuccess: 'TAG_GET_ALL',
        // }))
        dispatch(MyApi({
            url: 'admin/get/language',
            method: 'GET',
            onSuccess: 'GLOBAL_LANGUAGE',
        }))
    }, [])

    useEffect(()=>{
        if (success){
            form.resetFields();
        }
    }, [success])

    function beforeUpload(file){
        const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
        if (!isJpgOrPng) {
            message.error('You can only upload JPG/PNG file!');
        }
        const isLt2M = file.size / 1024 / 1024 < 2;
        if (!isLt2M) {
            message.error('Image must smaller than 2MB!');
        }
        return isJpgOrPng && isLt2M;
    }

    function handleChange(info){
        if (info.file.status === 'uploading') {

            setState({ ...state, loading: true, imageUrl: false  })
            // setState({ ...state, imageUrl: false });
            console.dir(info);
            return;

        }
        if (info.file.status === 'done') {

            console.dir(info);
            setState({...state, imageUrl: info.file.response.imageUrl, loading: false })
            dispatch({type: 'SET_MESSAGE', payload: info.file.response.message})

        }
        if (info.file.status === 'error'){
            dispatch({type: 'VALIDATION', payload: info.file.response.message})
        }
    }

    function onFinish(values){

        dispatch(MyApi({

            url: `admin/content/products`,
            method: 'POST',
            data: {
                ...values,
                img: values.image ? values.image.file.response.url : state.imageUrl,
            },

        }))
    }

    return (
        <>
            <Alerts span={18} offset={4} />

            <Form
                form={form}
                name="basic"
                labelCol={{ span: 4 }}
                wrapperCol={{ span: 18 }}
                initialValues={{ remember: true }}
                onFinish={onFinish}
                autoComplete="off"
            >
                {/*<div style={{ marginBottom: 10 }}>Название продукта:</div>*/}

                {/*{language &&*/}
                {/*language.map((item)=>(*/}
                {/*    <React.Fragment key={item.name}>*/}
                {/*        <Form.Item*/}
                {/*            label={item.name.toUpperCase()}*/}
                {/*            name={`title_${item.title}`}*/}
                {/*            rules={[{ required: true, message: 'Please input your password!' }]}*/}
                {/*        >*/}
                {/*            <Input />*/}
                {/*        </Form.Item>*/}
                {/*    </React.Fragment>*/}
                {/*))*/}
                {/*}*/}


                {/*<div style={{ marginBottom: 10 }}>Описание продукта:</div>*/}
                {/*{language &&*/}
                {/*language.map((item)=>(*/}
                {/*    <React.Fragment key={item.name}>*/}
                {/*        <Form.Item*/}
                {/*            label={item.name.toUpperCase()}*/}
                {/*            name={`description_${item.title}`}*/}
                {/*            rules={[{ required: true, message: 'Please input the product description!' }]}*/}
                {/*        >*/}
                {/*            <Input />*/}
                {/*        </Form.Item>*/}
                {/*    </React.Fragment>*/}
                {/*))*/}
                {/*}*/}

                <Form.Item
                    label="title"
                    name="title"
                    rules={[{ required: true, message: 'Please the title of the product' }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="description"
                    name="description"
                    rules={[{ required: true, message: 'Please the description of the product' }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="cost"
                    name='cost'
                    rules={[{required: true, message: 'Please input cost of the product!'}]}
                >
                    <InputNumber min={1} />
                </Form.Item>


                <Form.Item
                    label="image"
                    name='image'
                    rules={[{required: true, message: 'Please input title!'}]}
                >
                    <Upload
                        name='image'
                        listType="picture-card"
                        className="uploader_file"
                        showUploadList={false}

                        action="/api/admin/content/upload-image"
                        headers={{
                            Authorization: `Bearer ${localStorage.getItem('access_token')}`,
                            Accept: 'application/json'
                        }}
                        beforeUpload={beforeUpload}
                        onChange={handleChange}
                    >
                        {state.imageUrl
                            ? <img src={`/${state.imageUrl}`} alt="avatar" style={{ width: '100%' }} />
                            : (
                                <div>
                                    {state.loading ? <LoadingOutlined /> : <PlusOutlined />}
                                    <div style={{ marginTop: 8 }}>Upload</div>
                                </div>
                            )}
                    </Upload>
                </Form.Item>


                <Form.Item wrapperCol={{ offset: 4, span: 18 }}>
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        </>
    );
}

export default CreateProduct;
