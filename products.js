import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import MyApi from "../../../actions/MyApi";
import Checkbox from "antd/es/checkbox/Checkbox";
import {Button, Image, Space, Table, Tag} from "antd";
import {Link} from "react-router-dom";
import {route} from "../../../route/routeName";
import Alerts from "../../../Ui/Alerts";
import paginationHelper from "../../../helpers/pagination";

const Products = () => {
    const {products} = useSelector(state => state.product)
    const [page, setPage] = useState(1)
    const dispatch = useDispatch()
    console.log(products.data);

    useEffect(()=>{
        dispatch(MyApi({
            url: 'admin/content/products/',
            method: 'GET',
            onSuccess: 'PRODUCTS_GET_ALL',
            data:{page}
        }))
    }, [page])

    function onChangePage(page){
        setPage(page)
    }

    // function onChangePublished(status, id){
    //     dispatch(MyApi({
    //         url: `admin/content/products/${id}`,
    //         method: 'PUT',
    //         data: {status},
    //         onFilter:{
    //             type: 'PRODUCTS_CHANGE_PUBLISHED',
    //             data: {id, status}
    //         }
    //     }))
    // }

    function onDelete(id){
        dispatch(MyApi({
            url: `admin/content/products/${id}`,
            method: 'DELETE',
            onFilter:{
                type: 'PRODUCTS_DELETE',
                data: {id}
            }
        }))
    }

    const columns = [
        {
            title: 'id',
            dataIndex: 'id',
            width: '30px',
            fixed: 'left',
        },
        {
            title: 'name',
            dataIndex: 'name',
            align: 'left',
        },

        {
            title: 'img',
            dataIndex: 'img',
            align: 'center',
            width: '20%',
            render: (img)=>(
                <Image
                    width={200}
                    src={`/${img}`}
                />
            )
        },

        {
            title: 'description',
            dataIndex: 'description',
            align: 'left',
        },
        {
            title: 'cost',
            dataIndex: 'cost',
            align: 'center',
        },

        {
            title: 'Действия',
            dataIndex: 'id',
            key: 'action',
            align: 'center',
            render: (id) =>(
                <Space size="middle">
                    <Button type="primary">
                        <Link to={route.product.replace(':id', id)}>Подробней</Link>
                    </Button>
                    <Button type="primary"
                            danger
                            onClick={()=>{onDelete(id)}}>
                        Удалить
                    </Button>
                </Space>
            )
        },

    ];

    return (
        <>
            <Alerts />
            <Button type="primary"
                    style={{marginBottom: 10}}>
                <Link to={route.createProduct}>Добавить</Link>
            </Button>
            {products &&
            <Table
                scroll={{ x: 1024 }}
                columns={columns}
                rowKey={'id'}
                dataSource={products.data}
                size="middle"
                pagination={paginationHelper(products.meta.pagination, onChangePage)}
            />
            }
        </>
    );
};




export default Products;
