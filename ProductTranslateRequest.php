<?php

namespace App\Http\Requests\Admin\Content\Product;

use App\Http\Requests\BaseRequests;

class ProductTranslateRequest extends BaseRequests
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'title' => ['required', 'string'],
            'lang' => ['required', 'string', 'exists:language,title'],
        ];
    }
}
