import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import MyApi from "../../../actions/MyApi";
import {Button, Card, Form, Image, Input, InputNumber, Select, Upload} from "antd";
import Alerts from "../../../Ui/Alerts";
import {LoadingOutlined, PlusOutlined} from '@ant-design/icons';
// import {lang} from "../../../../lang/Website";

const Product = (props) => {
    const {product} = useSelector(state => state.product)

    // const {tags} = useSelector(state => state.tag)
    const [activeTabKey, setActiveTabKey] = useState('ru');
    const [activeDescTabKey, setActiveDescTabKey] = useState('ru');

    const [contentList, setContentList] = useState();
    const [contentDescList, setContentDescList] = useState();

    const [state, setState] = useState({
        loading: false,
        imageUrl: false,
    });

    const [tabList, setTabList] = useState();
    const [tabDescList, setTabDescList] = useState();

    const dispatch = useDispatch()

    useEffect(()=>{
        dispatch(MyApi({
            url: `admin/content/products/${props.match.params.id}`,
            method: 'GET',
            onSuccess: 'PRODUCT_GET_PRODUCT',
        }))

        // dispatch(MyApi({
        //     url: 'admin/content/tags/all',
        //     method: 'GET',
        //     onSuccess: 'TAG_GET_ALL',
        // }))

        return ()=>{
           dispatch({type:'PRODUCT_REMOVE_PRODUCT'})
        }
    }, [])

    useEffect(()=>{
        if (product){
            // начальная инициализация изображения
            setState({...state, imageUrl: product.img})
            let content = {}, tab = [];
            let descContent = {}, descTab = [];

            let lang_title = {
                ru: false,
                en: false,
                ua: false,
            };

            let lang_desc = {
                ru: false,
                en: false,
                ua: false,
            };


            if(product.translate.length != 0) {
                product.translate.map((item) => {
                    content = {
                        ...content,
                        [item.language]: <Form.Item
                            name={`lang_${item.language}`}
                            rules={[{required: true, message: 'Please input your email!'}]}
                            initialValue={item.name}
                        >
                            <Input/>
                        </Form.Item>
                    }
                    tab = [...tab, {key: item.language, tab: item.language.toUpperCase()}]

                    lang_title = {
                        ...lang_title,
                        [item.language] : true
                    }

                    descContent = {
                        ...descContent,
                        [item.language]: <Form.Item
                            name={`lang_${item.language}`}
                            rules={[{required: true, message: 'Please input your email!'}]}
                            initialValue={item.description}
                        >
                            <Input/>
                        </Form.Item>
                    }
                    descTab = [...descTab, {key: item.language, tab: item.language.toUpperCase()}]

                    lang_desc = {
                        ...lang_desc,
                        [item.language] : true
                    }
                })
            }

            for ( let lang in lang_title ) {
                if(!lang_title[lang]) {
                    content = {
                        ...content,
                        [lang]: <Form.Item
                            name={`lang_${lang}`}
                            rules={[{required: true, message: `Please enter product title in ${lang}`}]}
                            initialValue={null}
                        >
                            <Input/>
                        </Form.Item>
                    }
                    tab = [...tab, {key: lang, tab: lang.toUpperCase()}]
                }
            }

            for ( let lang in lang_desc ) {
                if(!lang_desc[lang]) {
                    descContent = {
                        ...descContent,
                        [lang]: <Form.Item
                            name={`lang_${lang}`}
                            rules={[{required: true, message: `Please enter product description in ${lang}`}]}
                            initialValue={null}
                        >
                            <Input/>
                        </Form.Item>
                    }
                    descTab = [...descTab, {key: lang, tab: lang.toUpperCase()}]
                }
            }

            setContentList(content)
            setTabList(tab)
            setContentDescList(descContent);
            setTabDescList(descTab);
        }
    }, [product])

    function onTabChange(key){
        setActiveTabKey(key);
    }

    function onDescTabChange(key){
        setActiveDescTabKey(key);
    }

    function onFinishTranslate(values){
        dispatch(MyApi({
            url: `admin/content/products/translate/${props.match.params.id}`,
            method: 'PUT',
            data: {
                title: values[`lang_${activeTabKey}`],
                lang: activeTabKey,
            },
        }))
    }

    function onFinishDescTranslate(values) {
        dispatch(MyApi({
            url: `admin/content/products/translate/${props.match.params.id}`,
            method: 'PUT',
            data: {
                description: values[`lang_${activeDescTabKey}`],
                lang: activeDescTabKey,
            },
        }))
    }

    // Сохранение изменений
    function onFinish(values){
        console.dir(values);
        dispatch(MyApi({
            url: `admin/content/products/${props.match.params.id}`,
            method: 'PUT',
            data: {
                name: values.title,
                img: values.image ? values.image.file.response.imageUrl : state.imageUrl,
                cost: values.cost
            },
        }))
    }

    function beforeUpload(file){
        const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
        if (!isJpgOrPng) {
            message.error('You can only upload JPG/PNG file!');
        }
        const isLt2M = file.size / 1024 / 1024 < 2;
        if (!isLt2M) {
            message.error('Image must smaller than 2MB!');
        }
        return isJpgOrPng && isLt2M;
    }

    function handleChange(info){

        if (info.file.status === 'uploading') {
            console.dir(info);
            setState({...state, loading: true, imageUrl: false })
            return;
        }
        if (info.file.status === 'done') {
            console.dir(info);
            setState({...state, imageUrl: info.file.response.imageUrl, loading: false })
            dispatch({type: 'SET_MESSAGE', payload: info.file.response.message})

        }
        if (info.file.status === 'error'){
            dispatch({type: 'VALIDATION', payload: info.file.response.message})

        }
        // setState({...state, loading: state.loading, imageUrl: state.imageUrl });
    }

    return (
        <>
            <Alerts />

            {contentList &&
            <Form
                name="basic"
                wrapperCol={{span: 24}}
                initialValues={{remember: true}}
                onFinish={onFinishTranslate}
                autoComplete="off"
            >
                <Card
                    title="Перевод названия продукта:"
                    style={{ width: '100%' }}
                    tabList={tabList}
                    activeTabKey={activeTabKey}
                    tabBarExtraContent={
                        <Button type="primary" htmlType="submit">
                            Сохранить
                        </Button>}
                    onTabChange={key => {
                        onTabChange(key);
                    }}
                >
                    {contentList[activeTabKey]}
                </Card>
                <Form.Item
                    name='id'
                    initialValue={props.match.params.id}
                    hidden={true}
                >
                    <Input />
                </Form.Item>
            </Form>}





            {contentDescList &&
            <Form
                name="basic"
                wrapperCol={{span: 24}}
                initialValues={{remember: true}}
                onFinish={onFinishDescTranslate}
                autoComplete="off"
            >
                <Card
                    title="Перевод описания продукта:"
                    style={{ width: '100%' }}
                    tabList={tabDescList}
                    activeTabKey={activeDescTabKey}
                    tabBarExtraContent={
                        <Button type="primary" htmlType="submit">
                            Сохранить
                        </Button>}
                    onTabChange={key => {
                        onDescTabChange(key);
                    }}
                >
                    {contentDescList[activeDescTabKey]}
                </Card>
                <Form.Item
                    name='id'
                    initialValue={props.match.params.id}
                    hidden={true}
                >
                    <Input />
                </Form.Item>
            </Form>}



            {product &&
            <Form
                name="basic"
                wrapperCol={{span: 24}}
                initialValues={{remember: true}}
                onFinish={onFinish}
                autoComplete="off"
            >
                <Card
                    title="Контент"
                    style={{ width: '100%', marginTop: '15px'}}
                    extra={
                        <Button type="primary" htmlType="submit">
                            Сохранить
                        </Button>}
                >
                    <Form.Item
                        name='id'
                        initialValue={props.match.params.id}
                        hidden={true}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label="Title"
                        name='title'
                        initialValue={product.name}
                        rules={[{required: true, message: 'Please input title!'}]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="cost"
                        name='cost'
                        initialValue={product.cost}
                        rules={[{required: true, message: 'Please input cost of the product!'}]}
                    >
                        <InputNumber min={1} />
                    </Form.Item>

                    <Form.Item
                        label="image"
                        name='image'
                        initialValue={state.imageUrl}
                        rules={[{required: true, message: 'Please input title!'}]}
                    >
                        <Upload
                            name='image'
                            listType="picture-card"
                            className="uploader_file"
                            showUploadList={false}
                            fileList={[state.imageUrl]}
                            action="/api/admin/content/upload-image"
                            headers={{
                                Authorization: `Bearer ${localStorage.getItem('access_token')}`,
                                Accept: 'application/json'
                            }}
                            beforeUpload={beforeUpload}
                            onChange={handleChange}
                        >
                            {state.imageUrl
                                ? <img src={`/${state.imageUrl}`} alt="avatar" style={{ width: '100%' }} />
                                : (
                                    <div>
                                        {state.loading ? <LoadingOutlined /> : <PlusOutlined />}
                                        <div style={{ marginTop: 8 }}>Upload</div>
                                    </div>
                                )}
                        </Upload>
                    </Form.Item>

                </Card>
            </Form>
            }
        </>
    );
};

export default Product;
